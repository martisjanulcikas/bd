using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class BallsManager : MonoBehaviour
{
    public TextMeshProUGUI bulletText;
    public int bullets = 20;
    void Update()
    {
        bulletText.text = "X" + bullets.ToString();
        if (bullets <= 0)
        {
            Invoke("GameOver", 1f);
        }
    }
    void GameOver()
    {
        if (bullets <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
    public void AddBullets(int amount)
    {
        bullets += amount;
    }
}
