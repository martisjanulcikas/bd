using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFigure : MonoBehaviour
{
    public float speed = 3f;
    float dirX, dirY, dirZ;
    public float destroyTime = 8f;
    public Color[] colors;
    public int colorId;
    Renderer rend;
    void Awake()
    {
        rend = GetComponent<Renderer>();
        colorId = Random.Range(0, colors.Length);
        rend.material.color = colors[colorId];

        dirX = Random.Range(-speed, speed);
        dirY = Random.Range(-speed, speed);
        dirZ = Random.Range(-speed, speed);
        Destroy(gameObject, 5f);
    }
    void Update()
    {
        this.transform.Translate(dirX * Time.deltaTime,
                        dirY * Time.deltaTime,
                        dirZ * Time.deltaTime);
    }
}
