using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Shooting : MonoBehaviour
{
    public Transform arCamera;
    public AudioClip shootClip;
    public GameObject bullet;
    public float shootVolume = 0.7f;
    AudioSource audioSrc;
    public float speed = 7f;
    void Awake()
    {
        audioSrc = this.GetComponent<AudioSource>();
    }
    public void Shoot()
    {
        if (this.GetComponent<BallsManager>().bullets > 0)
        {
            ShootBullet();
            this.GetComponent<BallsManager>().AddBullets(-1);
            audioSrc.PlayOneShot(shootClip);
            audioSrc.volume = shootVolume;
        }
    }
    void ShootBullet()
    {
        GameObject bull = Instantiate(bullet,
                                      arCamera.transform.position,
                                      arCamera.transform.rotation) as GameObject;

        bull.GetComponent<Rigidbody>().AddForce(arCamera.forward * speed * 100f);
    }
}
