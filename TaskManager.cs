using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TaskManager : MonoBehaviour
{
    public Image[] images;
    public Color[] colors;
    int randColor;
    int randImage;
    public float changeImageTime = 0.5f;
    public int taskColorId;
    public int taskFigureId;

    void Start()
    {
        StartCoroutine(ChangeTask());
    }

    IEnumerator ChangeTask()
    {
        for (int i = 0; i < images.Length; i++)
        {
            images[i].enabled = false;
        }

        randImage = Random.Range(0, images.Length);
        randColor = Random.Range(0, colors.Length);

        taskColorId = randColor;
        taskFigureId = randImage;

        images[randImage].enabled = true;
        images[randImage].color = colors[randColor];

        yield return new WaitForSeconds(changeImageTime);
        StartCoroutine(ChangeTask());
    }
}
