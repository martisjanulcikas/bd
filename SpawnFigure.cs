using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFigure : MonoBehaviour
{
    public GameObject[] figures;
    public int figuresPerTime = 2;
    public int spawnTime = 2;
    public float minRange = -5;
    public float maxRange = 5;
    public int spatnPointsPerTime = 2;
    void Start()
    {
        StartCoroutine(SpawnFigures());
    }
    IEnumerator SpawnFigures()
    {
        for (int i = 0; i < spatnPointsPerTime; i++)
        {
            Vector3 figurePos = new Vector3(Random.Range(minRange, maxRange),
                                            Random.Range(minRange * 0.7f, maxRange * 0.7f),
                                            Random.Range(minRange * 0.2f, maxRange));
            for (int j = 0; j < figuresPerTime; j++)
            {
                int randomFigureIndex = Random.Range(0, figures.Length);
                Instantiate(figures[randomFigureIndex], figurePos, Quaternion.identity);
            }
        }

        yield return new WaitForSeconds(spawnTime);

        StartCoroutine(SpawnFigures());
    }


}
