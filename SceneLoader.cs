using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneLoader : MonoBehaviour
{
    public float loadingTime = 2f;
    int scene;
    void Start()
    {
        scene = SceneManager.GetActiveScene().buildIndex;
        Invoke("Load", loadingTime);
    }
    void Load()
    {
        if (scene == 0)
        {
            SceneManager.LoadScene(scene + 1);
        }
    }
    public void LoadNextScene()
    {
        SceneManager.LoadScene(scene + 1);
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void LoadMenuScene()
    {
        SceneManager.LoadScene(1);
    }
}
