using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class ScoreManager : MonoBehaviour
{
    public int score = 0;
    int taskColorId = -1;
    int taskFigureId = -1;
    public TextMeshProUGUI scoreText;
    public GameObject taskManager;
    void Update()
    {
        scoreText.text = "SCORE: " + score.ToString();
    }
    public void GetScore(int colorId, int figureId)
    {
        taskColorId = taskManager.GetComponent<TaskManager>().taskColorId;
        taskFigureId = taskManager.GetComponent<TaskManager>().taskFigureId;

        if (colorId == taskColorId && figureId == taskFigureId)
        {
            score += 100;
            this.GetComponent<BallsManager>().AddBullets(2);
        }
        else
        {
            if (colorId == taskColorId || figureId == taskFigureId)
            {
                score += 25;
                this.GetComponent<BallsManager>().AddBullets(1);
            }
            else
            {
                if (score - 25 <= 0)
                {
                    score = 0;
                }
                else
                {
                    score -= 25;
                }
                this.GetComponent<BallsManager>().AddBullets(-2);
            }
        }
    }
}
