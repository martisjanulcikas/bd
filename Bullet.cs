using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float destroyTime = 6f;
    void Awake()
    {
        Destroy(gameObject, destroyTime);
    }
}
