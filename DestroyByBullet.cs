using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBullet : MonoBehaviour
{
    public AudioClip destroyClip;
    int figureId;
    void Awake()
    {
        if (this.GetComponent<MeshFilter>().mesh.name == "Capsule Instance")
            figureId = 0;
        if (this.GetComponent<MeshFilter>().mesh.name == "Sphere Instance")
            figureId = 1;
        if (this.GetComponent<MeshFilter>().mesh.name == "Cube Instance")
            figureId = 2;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            int colorId = this.GetComponent<MoveFigure>().colorId;
            GameObject.FindGameObjectWithTag("Player").GetComponent<ScoreManager>().GetScore(colorId, figureId);
            Destroy(other.gameObject);
            this.GetComponent<AudioSource>().PlayOneShot(destroyClip);
            Destroy(gameObject, 0.1f);
        }
    }
}
