using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GmaeOverUI : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    ScoreKeeper sm;
    void Awake()
    {
        sm = FindObjectOfType<ScoreKeeper>();
    }
    void Start()
    {
        scoreText.text = "Your Score: " + sm.score.ToString();
    }
}
